library(DESeq2)
library("pheatmap")
library("RColorBrewer")
library("Rsamtools")
library("PoiClaClu")

setwd('/path/to/deseq2')

sampleTable <- read.csv('sample_table.csv')
filenames <- paste0(sampleTable$Run, "_starAligned.sortedByCoord.out.bam")
file.exists(filenames)
bamfiles <- BamFileList(filenames)
seqinfo(bamfiles[1])
#### Defining gene models
library("GenomicFeatures")
txdb <- makeTxDbFromGFF('/mnt/gtklab01/siting/tham/Homo_sapiens.GRCh38.84.gtf', format="gtf", circ_seqs=character())
ebg <- exonsBy(txdb, by="gene")
#### Read counting step
library("GenomicAlignments")
library("BiocParallel")
register(MulticoreParam())
se <- summarizeOverlaps(features=ebg, reads=bamfiles,
                        mode="Union",
                        singleEnd=FALSE,
                        ignore.strand=FALSE,
                        fragments=TRUE )

colnames(se) <- colData(se)$SampleName
colData(se) <- DataFrame(sampleTable)
################### Visualization analysis
dds <- DESeqDataSet(se, design = ~ 1)
dds <- dds[rowSums(counts(dds)) > 1, ]
rld <- rlog(dds, blind=FALSE)

par(mfrow = c(1, 2))
dds <- estimateSizeFactors(dds)
plot(log2(counts(dds, normalized=TRUE)[,1:2] + 1), pch=16, cex=0.3)
plot(assay(rld)[,1:2], pch=16, cex=0.3)

sampleDists <- dist(t(assay(rld)))
sampleDistMatrix <- as.matrix(sampleDists)
rownames(sampleDistMatrix) <- rld$SampleName
colnames(sampleDistMatrix) <- rld$SampleName
colors <- colorRampPalette( rev(brewer.pal(9, "Blues")) )(255)
png("SampleCorrelation.png", 600, 600)

pheatmap(sampleDistMatrix,
         clustering_distance_rows=sampleDists,
         clustering_distance_cols=sampleDists,
         col=colors)
dev.off()


poisd <- PoissonDistance(t(counts(dds)))
samplePoisDistMatrix <- as.matrix( poisd$dd )
rownames(samplePoisDistMatrix) <- rld$SampleName
colnames(samplePoisDistMatrix) <- rld$SampleName
png("SampleCorrelation_poissonDistance.png", 800, 800)
pheatmap(samplePoisDistMatrix,
         clustering_distance_rows=poisd$dd,
         clustering_distance_cols=poisd$dd,
         col=colors)
dev.off()

png("PCAplot.png", 600, 600)
plotPCA(rld, intgroup = c("SampleName", "cell"))
dev.off()

library(ggplot2)
png('MDSplot.png', 600, 600)
mdsData <- data.frame(cmdscale(sampleDistMatrix))
mds <- cbind(mdsData, as.data.frame(colData(rld)))
ggplot(mds, aes(X1,X2,color=SampleName,shape=cell)) + geom_point(size=3) + theme(text = element_text(size = 16))
dev.off()
png("MDS_possionPlot.png", 600, 600)
mdsPoisData <- data.frame(cmdscale(samplePoisDistMatrix))
mdsPois <- cbind(mdsPoisData, as.data.frame(colData(dds)))
ggplot(mdsPois, aes(X1,X2,color=SampleName,shape=cell)) + geom_point(size=3) + theme(text = element_text(size = 16))
dev.off()

################# Differential expression
head(assay(rld), 5)
r <- as.data.frame(assay(rld))
###annotation
################# Annotation
library("AnnotationDbi")
library("org.Hs.eg.db")
library("ReportingTools")
columns(org.Hs.eg.db)
r$symbol <- mapIds(org.Hs.eg.db,
                     keys=row.names(r),
                     column="SYMBOL",
                     keytype="ENSEMBL",
                     multiVals="first")
r$entrez <- mapIds(org.Hs.eg.db,
                     keys=row.names(r),
                     column="ENTREZID",
                     keytype="ENSEMBL",
                     multiVals="first")
out <- write.table(r, "deseq2_count.txt", sep = '\t', quote = F, row.names = T)

################## Gene clustering
library("genefilter")
topVarGenes <- head(order(rowVars(assay(rld)),decreasing=TRUE),30)
mat <- assay(rld)[ topVarGenes, ]
mat <- mat - rowMeans(mat)
df <- as.data.frame(colData(rld)[,c("cell","SampleName")])
png("heatmap_top30highestVarianceCluster.png", 800, 800)
pheatmap(mat, annotation_col=df, fontsize = 16)
dev.off()
