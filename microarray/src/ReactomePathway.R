##### Pathway analysis using reactomeRA
source("src/setup.R")

library(ReactomePA)
library(graphite)
library(hugene10sttranscriptcluster.db)

lines <- c("MCF10F", "MCF7")
for (cell.line in lines ) {
  data <- read.table(paste(results.dir, "/","DEG_",cell.line,".tab",sep=""),
                     header=T, sep="\t")
  ID <- data$entrezgene
  pathway <- enrichPathway(gene=ID,pvalueCutoff=0.05, qvalueCutoff=0.1, readable=T)
  print(sprintf("Top 6 Pathways of %s",cell.line))
  print(head(summary(pathway)))
  png(resultsfile(paste("pathway_",cell.line,".png",sep="")),800,800)
  cnetplot(pathway, showCategory=4)
  dev.off()
}  
  
  
  